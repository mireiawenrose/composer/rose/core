<?php
declare(strict_types = 1);

namespace Rose\Framework;

use FastRoute\DataGenerator\GroupCountBased;
use FastRoute\RouteParser\Std;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger;
use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;
use Rose\Framework\Error\ModuleDoesNotExist;
use Rose\Framework\Error\ModuleExists;
use function _;

/**
 * The Rose Framework core
 *
 * @package Rose\Framework
 */
class Core
{
	/**
	 * Registered modules
	 *
	 * @var ModuleInterface[]
	 */
	protected array $modules;
	
	/**
	 * Configuration file handler
	 *
	 * @var Configuration
	 */
	protected Configuration $configuration;
	
	/**
	 * PSR-3 logger interface
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $logger;
	
	/**
	 * Set up the core
	 *
	 * @param Configuration $configuration
	 * @param LoggerInterface|null $logger
	 */
	public function __construct(Configuration $configuration, ?LoggerInterface $logger = NULL)
	{
		$this->configuration = $configuration;
		
		if ($logger === NULL)
		{
			$logger = new Logger(self::class);
		}
		else if ($logger instanceof Logger)
		{
			$logger = $logger->withName(self::class);
		}
		
		if ($logger instanceof Logger)
		{
			$handler = new StreamHandler('php://stdout', Level::Debug);
			$formatter = new LineFormatter('[%datetime%][%channel%] %level_name%: %message%' . "\n");
			$handler->setFormatter($formatter);
			$logger->pushHandler($handler);
		}
		
		$this->logger = $logger;
		
		$this->logger->debug(_('Core initialized'));
	}
	
	/**
	 * Register a new module
	 *
	 * @param ModuleInterface $module
	 *    The module to register
	 *
	 * @throws ModuleExists
	 *    In case module is already loaded
	 */
	public function Register(ModuleInterface $module) : void
	{
		if ($this->Has($module->GetName()))
		{
			throw new ModuleExists();
		}
		
		$this->modules[$module->GetName()] = $module;
	}
	
	/**
	 * Check if a module is loaded
	 *
	 * @param string $module
	 *    The module name to check for
	 *
	 * @return bool
	 *    Status of the requested module
	 */
	public function Has(string $module) : bool
	{
		return isset($this->modules[$module]);
	}
	
	/**
	 * Get a module from loaded modules
	 *
	 * @param string $module
	 *    The module name to get
	 *
	 * @return ModuleInterface
	 *    The instance of the loaded module
	 *
	 * @throws ModuleDoesNotExist
	 *    In case the module is not loaded
	 */
	public function Get(string $module) : ModuleInterface
	{
		if (!$this->Has($module))
		{
			throw new ModuleDoesNotExist();
		}
		
		return $this->modules[$module];
	}
	
	/**
	 * Get the configuration interface
	 *
	 * @return Configuration
	 */
	public function Configuration() : Configuration
	{
		return $this->configuration;
	}
	
	/**
	 * Get the logging instance
	 *
	 * @return AbstractLogger
	 */
	public function Logger() : LoggerInterface
	{
		return $this->logger;
	}
}