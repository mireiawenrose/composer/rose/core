<?php
declare(strict_types = 1);

namespace Rose\Framework;

/**
 * Basic interface for the modules
 *
 * @package Rose\Framework
 */
interface ModuleInterface
{
	/**
	 * Get the name of the module
	 *
	 * @return string
	 */
	public function GetName() : string;
}