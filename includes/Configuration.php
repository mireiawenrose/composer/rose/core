<?php
declare(strict_types = 1);

namespace Rose\Framework;

use Mireiawen\Configuration\InvalidFile;
use Mireiawen\Configuration\JSON;
use Mireiawen\Configuration\MissingExtension;
use Mireiawen\Configuration\MissingValue;
use Mireiawen\Configuration\YAML;
use Mireiawen\Input\AbstractInput;
use Mireiawen\Input\Env;

class Configuration extends AbstractInput
{
	/**
	 * The environment variables available
	 *
	 * @var Env
	 */
	protected Env $env;
	
	/**
	 * The configuration file interface
	 *
	 * @var \Mireiawen\Configuration\Configuration
	 */
	protected \Mireiawen\Configuration\Configuration $configuration;
	
	/**
	 * Initialize the configuration class, and load YAML configuration file
	 *
	 * @param string $filename
	 *    The configuration file to load
	 *
	 * @return self
	 *
	 * @throws MissingExtension
	 *    In case of a required extension is missing
	 *
	 * @throws InvalidFile
	 *    In case of unable to read or process the configuration file
	 */
	public static function LoadYAML(string $filename) : self
	{
		return new self(new YAML($filename));
	}
	
	/**
	 * Initialize the configuration class, and load JSON configuration file
	 *
	 * @param string $filename
	 *    The configuration file to load
	 *
	 * @return self
	 *
	 * @throws MissingExtension
	 *    In case of a required extension is missing
	 *
	 * @throws InvalidFile
	 *    In case of unable to read or process the configuration file
	 */
	public static function LoadJSON(string $filename) : self
	{
		return new self(new JSON($filename));
	}
	
	/**
	 * Initialize the configuration class
	 *
	 * @param \Mireiawen\Configuration\Configuration $configuration
	 *    The configuration file to use
	 */
	public function __construct(\Mireiawen\Configuration\Configuration $configuration)
	{
		$this->env = new Env();
		$this->configuration = $configuration;
	}
	
	/**
	 * Load the configuration file
	 *
	 * @param string $filename
	 *    The configuration file to load
	 *
	 * @throws InvalidFile
	 *    In case of unable to read or process the configuration file
	 */
	public function Load(string $filename) : void
	{
		$this->configuration->Load($filename);
	}
	
	/**
	 * Check if the configuration is loaded
	 *
	 * @return bool
	 */
	public function IsLoaded() : bool
	{
		return $this->configuration->IsLoaded();
	}
	
	/**
	 * Check if the configuration variable exists and is set
	 *
	 * @param string $key
	 *    The key to check
	 *
	 * @return bool
	 *    TRUE if the value exists and is set,
	 *    FALSE if the value does not exist
	 */
	public function Has(string $key) : bool
	{
		if ($this->env->Has($this->ParseEnvKey($key)))
		{
			return TRUE;
		}
		
		return $this->configuration->Has($key);
	}
	
	/**
	 * Get the configuration variable and return its value, or default if it
	 * is not set
	 *
	 * @param string $key
	 *    The key to retrieve
	 *
	 * @param mixed $default
	 *    The default value for the key, NULL to throw error if key is not set
	 *
	 * @return mixed
	 *    The value of the key
	 *
	 * @throws MissingValue
	 *    If the key is not set and no default value is specified
	 */
	public function Get(string $key, $default = NULL) : mixed
	{
		if ($this->env->Has($this->ParseEnvKey($key)))
		{
			return $this->env->Get($key, $default);
		}
		
		return $this->configuration->Get($key, $default);
	}
	
	
	/**
	 * Set the input variable
	 *
	 * @param string $key
	 *    The name of the key to write
	 *
	 * @param mixed $value
	 *    The value to write
	 */
	public function Set(string $key, $value) : void
	{
		$this->env->Set($key, $value);
	}
	
	/**
	 * Parse the key to variable compatible with the env names
	 *
	 * @param string $key
	 * The key to parse
	 *
	 * @return string
	 *    The parsed key
	 */
	protected function ParseEnvKey(string $key) : string
	{
		return str_replace('/', '__', $key);
	}
}