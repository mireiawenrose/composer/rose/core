<?php
declare(strict_types = 1);

namespace Rose\Framework\Error;

use RuntimeException;
use Throwable;
use function _;

/**
 * Module is already registered exception
 *
 * @package Rose\Framework
 */
class ModuleDoesNotExist extends RuntimeException
{
	public function __construct(string $message = '', int $code = 0, ?Throwable $previous = NULL)
	{
		if (empty($message))
		{
			$message = _('Module is not registered');
		}
		
		parent::__construct($message, $code, $previous);
	}
}