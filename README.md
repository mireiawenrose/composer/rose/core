# Rose Framework Core
Simple and lightweight PHP framework for modern PHP.

There will be plugins to handle more requirements, such as templates.

## Requirements
- PHP 8.3
- ext-gettext
- ext-json (configuration dependency)
- ext-yaml (configuration dependency)

## Features
The core tries to be as minimal as possible, with the minimalistic approach to
include the almost always needed pieces for web application development.

### Configuration
Support for environment variables, YAML and JSON files.

### Logging
[PSR-3](https://www.php-fig.org/psr/psr-3/) compatible logging interface,
defaults to [Monolog](https://github.com/Seldaek/monolog).

### Routing
Loosely tied to [FastRoute](https://github.com/nikic/FastRoute), other
implementations would likely be possible as well.
